# How to use a Discovery File to create an API Resource Discovery Endpoint

All ASPSPs who have a Functional Conformance Certification published by OBIE, will have their [Discovery Files](https://bitbucket.org/openbankingteam/conformance-discovery/src/master/aspsp/) hosted by OBIE.

Regardless, any ASPSP can also create their own Discovery File using this [Discovery Specification](https://bitbucket.org/openbankingteam/conformance-suite/src/develop/docs/discovery.md)

This specification also explains how to use this Discovery File as an API Resource Discovery Endpoint. 